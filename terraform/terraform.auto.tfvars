region = "us-west-2"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

enable_classiclink = "false"

enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = "ami-0072e79f5fde88bf0"

ami-bastion = "ami-0756b639f1558c992"

ami-nginx = "ami-07b3ccd97bdb08c72"

ami-sonar = ""

keypair = "devops"

master-password = "devopspblproject"

master-username = "ola"

account_no = "486356681924"

tags = {
  Owner-Email     = "olaminiyi@gmail.com"
  Managed-By      = "Terraform"
  Billing-Account = "1234567890"
}